package com.tubes.tokoFurnitur.repository;

import org.springframework.data.repository.CrudRepository;

import com.tubes.tokoFurnitur.model.Payment;

public interface PaymentRepository extends CrudRepository<Payment, Integer> {
    
}