package com.tubes.tokoFurnitur.repository;

import org.springframework.data.repository.CrudRepository;

import com.tubes.tokoFurnitur.model.ProductCategory;

public interface ProductCategoryRepository extends CrudRepository<ProductCategory, Integer> {
    
}