package com.tubes.tokoFurnitur.repository;

import org.springframework.data.repository.CrudRepository;

import com.tubes.tokoFurnitur.model.Shipment;

public interface ShipmentRepository extends CrudRepository<Shipment, Integer> {
    
}