package com.tubes.tokoFurnitur.repository;

import org.springframework.data.repository.CrudRepository;

import com.tubes.tokoFurnitur.model.Orders;

public interface OrdersRepository extends CrudRepository<Orders, Integer> {
    
}