package com.tubes.tokoFurnitur.repository;

import org.springframework.data.repository.CrudRepository;

import com.tubes.tokoFurnitur.model.ProductReview;

public interface ProductReviewRepository extends CrudRepository<ProductReview, Integer> {
    
}