package com.tubes.tokoFurnitur.repository;

import org.springframework.data.repository.CrudRepository;

import com.tubes.tokoFurnitur.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    
}