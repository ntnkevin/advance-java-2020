package com.tubes.tokoFurnitur.repository;

import org.springframework.data.repository.CrudRepository;

import com.tubes.tokoFurnitur.model.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {
    
}