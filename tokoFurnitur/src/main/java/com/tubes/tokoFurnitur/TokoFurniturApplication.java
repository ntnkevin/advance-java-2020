package com.tubes.tokoFurnitur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TokoFurniturApplication {

	public static void main(String[] args) {
		SpringApplication.run(TokoFurniturApplication.class, args);
	}

}
