package com.tubes.tokoFurnitur.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tubes.tokoFurnitur.model.Customer;
import com.tubes.tokoFurnitur.repository.CustomerRepository;

@Controller
@RequestMapping(path="/tubes")
public class CustomerController {
  @Autowired
         
  private CustomerRepository customerRepository;

  @PostMapping(path="/customer/add") 
  public @ResponseBody ResponseEntity<Customer> addNewCustomer (
      @RequestParam String name,
      @RequestParam String address,
      @RequestParam String gender,
      @RequestParam String phone      
      ) {
    
    try {
    	Customer customerData = new Customer();
    	customerData.setName(name);
        customerData.setAddress(address);
        customerData.setGender(gender);
        customerData.setPhone(phone);
        customerRepository.save(customerData);
        return ResponseEntity.ok(customerData); 
	}catch (Exception e) {
		return ResponseEntity.notFound().build();
	}
  }

  @GetMapping(path="/customer/showall")
  public @ResponseBody ResponseEntity<Iterable<Customer>> getAllCustomer() {
	  try {
			return ResponseEntity.ok(customerRepository.findAll()); 
		}catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
  }

  @GetMapping(path="/customer/{customerid}")
  public @ResponseBody ResponseEntity<Customer> getCustomer(@PathVariable int customerid) {
	try {
		Customer customerData = customerRepository.findById(customerid).get();
		return ResponseEntity.ok(customerData); 
	}catch (Exception e) {
		return ResponseEntity.notFound().build();
	}
  }
  
  @PutMapping(path="/customer/update/{customerid}")
  public @ResponseBody ResponseEntity<Customer> updateCustomer(
        @RequestParam String name,
        @RequestParam String address,
        @RequestParam String gender,
        @RequestParam String phone ,
	    @PathVariable int customerid) {

	  try {
        Customer customerData = new Customer();
        customerData.setCustomerId(customerid);
    	customerData.setName(name);
        customerData.setAddress(address);
        customerData.setGender(gender);
        customerData.setPhone(phone);
		  customerRepository.save(customerData);
		  return ResponseEntity.ok().build();
		}catch (Exception e) {
		  return ResponseEntity.notFound().build();
		}
  }
  
  @DeleteMapping(path="/customer/delete/{customerid}")
  public @ResponseBody ResponseEntity<Void> deteleCustomer(@PathVariable int customerid){
	try {
    customerRepository.deleteById(customerid);
	  return ResponseEntity.ok().build();
	}catch (Exception e) {
	  return ResponseEntity.notFound().build();
	} 
  }
}