package com.tubes.tokoFurnitur.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
// import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tubes.tokoFurnitur.model.Orders;
import com.tubes.tokoFurnitur.model.ProductReview;
import com.tubes.tokoFurnitur.repository.OrdersRepository;
import com.tubes.tokoFurnitur.repository.ProductReviewRepository;

@Controller
@RequestMapping(path="/tubes")
public class ProductReviewController {
  @Autowired
         
  private ProductReviewRepository productReviewRepository;
  @Autowired
  private OrdersRepository ordersRepository;
  @PostMapping(path="/productreview/add") 
  public @ResponseBody ResponseEntity<ProductReview> addNewProductReview (
        @RequestParam String reviewdate,
        @RequestParam String description,
        @RequestParam Integer ordersid
      ) {
    
    try {
    	ProductReview prData = new ProductReview();
    	Orders orders = ordersRepository.findById(ordersid).get();
        prData.setOrdersid(orders);
        prData.setReviewDate(reviewdate);
        prData.setDescription(description);
        
        productReviewRepository.save(prData);
        return ResponseEntity.ok(prData); 
	}catch (Exception e) {
		return ResponseEntity.notFound().build();
	}
  }

  @GetMapping(path="/productreview/showall")
  public @ResponseBody ResponseEntity<Iterable<ProductReview>> getAllProductReview() {
	  try {
			return ResponseEntity.ok(productReviewRepository.findAll()); 
		}catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
  }

  @GetMapping(path="/productreview/{reviewid}")
  public @ResponseBody ResponseEntity<ProductReview> getProductReview(@PathVariable int reviewid) {
	try {
		ProductReview prData = productReviewRepository.findById(reviewid).get();
		return ResponseEntity.ok(prData); 
	}catch (Exception e) {
		return ResponseEntity.notFound().build();
	}
  }
  
  @PutMapping(path="/productreview/update/{reviewid}")
  public @ResponseBody ResponseEntity<ProductReview> updateProductReview(
      @PathVariable int reviewid,
      @RequestParam String reviewdate,
      @RequestParam String description,
      @RequestParam Integer ordersid) {

	  try {
        ProductReview prData = new ProductReview();
    	Orders orders = ordersRepository.findById(ordersid).get();
    	
        prData.setReviewId(reviewid);
        prData.setOrdersid(orders);
        prData.setReviewDate(reviewdate);
        prData.setDescription(description);

        productReviewRepository.save(prData);
		  return ResponseEntity.ok().build();
		}catch (Exception e) {
		  return ResponseEntity.notFound().build();
		}
  }
  //gaada delete karena review

//   @DeleteMapping(path="/productreview/{reviewid}")
//   public @ResponseBody ResponseEntity<Void> deteleProductReview(@PathVariable int ordersid){
// 	try {
// 	  ordersRepository.deleteById(ordersid);
// 	  return ResponseEntity.ok().build();
// 	}catch (Exception e) {
// 	  return ResponseEntity.notFound().build();
// 	} 
//   }
}