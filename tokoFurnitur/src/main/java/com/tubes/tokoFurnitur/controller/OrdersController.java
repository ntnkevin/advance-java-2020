package com.tubes.tokoFurnitur.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tubes.tokoFurnitur.model.Customer;
import com.tubes.tokoFurnitur.model.Orders;
import com.tubes.tokoFurnitur.model.Product;
import com.tubes.tokoFurnitur.repository.CustomerRepository;
import com.tubes.tokoFurnitur.repository.OrdersRepository;
import com.tubes.tokoFurnitur.repository.ProductRepository;

@Controller
@RequestMapping(path="/tubes")
public class OrdersController {
  @Autowired
         
  private OrdersRepository ordersRepository;
  @Autowired
  private ProductRepository productRepository;
  @Autowired
  private CustomerRepository customerRepository;
  @PostMapping(path="/orders/add") 
  public @ResponseBody ResponseEntity<Orders> addNewOrders (
      @RequestParam String ordersdate,
      @RequestParam Integer qty,
      @RequestParam Integer productid,
      @RequestParam String status,
      @RequestParam Integer customerid
      ) {
    
    try {
    	Orders ordersData = new Orders();
    	Product prod = productRepository.findById(productid).get();
    	Customer cust = customerRepository.findById(customerid).get();
    	ordersData.setOrdersDate(ordersdate);
        ordersData.setQty(qty);
        ordersData.setProductid(prod);
        ordersData.setCustomerid(cust);
        ordersData.setStatus(status);
        
        ordersRepository.save(ordersData);
        return ResponseEntity.ok(ordersData); 
	}catch (Exception e) {
		return ResponseEntity.notFound().build();
	}
  }

  @GetMapping(path="/orders/showall")
  public @ResponseBody ResponseEntity<Iterable<Orders>> getAllOrders() {
	  try {
			return ResponseEntity.ok(ordersRepository.findAll()); 
		}catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
  }

  @GetMapping(path="/orders/{ordersid}")
  public @ResponseBody ResponseEntity<Orders> getOrders(@PathVariable int ordersid) {
	try {
		Orders ordersData = ordersRepository.findById(ordersid).get();
		return ResponseEntity.ok(ordersData); 
	}catch (Exception e) {
		return ResponseEntity.notFound().build();
	}
  }
  
  @PutMapping(path="/orders/update/{ordersid}")
  public @ResponseBody ResponseEntity<Orders> updateOrders(
      @PathVariable int ordersid,
      @RequestParam String ordersdate,
      @RequestParam String status,
      @RequestParam Integer qty,
      @RequestParam Integer productid,
      @RequestParam Integer customerid) {

	  try {
        Orders ordersData = new Orders();
        Product prod = productRepository.findById(productid).get();
        Customer cust = customerRepository.findById(customerid).get();
        ordersData.setOrdersId(ordersid);
    	ordersData.setOrdersDate(ordersdate);
        ordersData.setQty(qty);
        ordersData.setProductid(prod);
        ordersData.setCustomerid(cust);
        ordersData.setStatus(status);

        ordersRepository.save(ordersData);
		  return ResponseEntity.ok().build();
		}catch (Exception e) {
		  return ResponseEntity.notFound().build();
		}
  }
  
  @DeleteMapping(path="/orders/delete/{ordersid}")
  public @ResponseBody ResponseEntity<Void> deteleOrders(@PathVariable int ordersid){
	try {
	  ordersRepository.deleteById(ordersid);
	  return ResponseEntity.ok().build();
	}catch (Exception e) {
	  return ResponseEntity.notFound().build();
	} 
  }
}