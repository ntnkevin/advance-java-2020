package com.tubes.tokoFurnitur.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tubes.tokoFurnitur.model.Product;
import com.tubes.tokoFurnitur.model.ProductCategory;
import com.tubes.tokoFurnitur.repository.ProductCategoryRepository;
import com.tubes.tokoFurnitur.repository.ProductRepository;


@Controller
@RequestMapping(path="/tubes")
public class ProductController {
  @Autowired
         
  private ProductRepository productRepository;
  @Autowired
  private ProductCategoryRepository productcategoryRepository;
  @PostMapping(path="/product/add") 
  public @ResponseBody ResponseEntity<Product> addNewProduct (
      @RequestParam String name,
      @RequestParam String description,
      @RequestParam String color,
      @RequestParam Integer price,
      @RequestParam int categoryid
      ) {
    
    try {
    	Product productData = new Product();
    	ProductCategory pcategory = productcategoryRepository.findById(categoryid).get();
    	productData.setCategoryid(pcategory);
    	productData.setName(name);
        productData.setDescription(description);
        productData.setPrice(price);
        productData.setColor(color);
        productRepository.save(productData);
        return ResponseEntity.ok(productData); 
	}catch (Exception e) {
		return ResponseEntity.notFound().build();
	}
  }

  @GetMapping(path="/product/showall")
  public @ResponseBody ResponseEntity<Iterable<Product>> getAllProduct() {
	  try {
			return ResponseEntity.ok(productRepository.findAll()); 
		}catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
  }
  
  @GetMapping(path="/product/{productid}")
  public @ResponseBody ResponseEntity<Product> getProduct(@PathVariable int productid) {
	try {
		Product productData = productRepository.findById(productid).get();
		return ResponseEntity.ok(productData); 
	}catch (Exception e) {
		return ResponseEntity.notFound().build();
	}
  }
  

  
  @PutMapping(path="/product/update/{productid}")
  public @ResponseBody ResponseEntity<Product> updateProduct(
      @PathVariable int productid,
    @RequestParam String name,
    @RequestParam String description,
    @RequestParam String color,
    @RequestParam Integer price,
    @RequestParam Integer categoryid) {

	  try {
        Product productData = new Product();
        productData.setProductId(productid);
    	productData.setName(name);
        productData.setDescription(description);
        productData.setPrice(price);
        productData.setColor(color);
        productRepository.save(productData);
		  return ResponseEntity.ok().build();
		}catch (Exception e) {
		  return ResponseEntity.notFound().build();
		}
  }
  
  @DeleteMapping(path="/product/delete/{productid}")
  public @ResponseBody ResponseEntity<Void> deteleProductCategory(@PathVariable int productid){
	try {
	  productRepository.deleteById(productid);
	  return ResponseEntity.ok().build();
	}catch (Exception e) {
	  return ResponseEntity.notFound().build();
	} 
  }
}