package com.tubes.tokoFurnitur.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tubes.tokoFurnitur.model.ProductCategory;
import com.tubes.tokoFurnitur.repository.ProductCategoryRepository;

@Controller
@RequestMapping(path="/tubes")
public class ProductCategoryController {
  @Autowired
         
  private ProductCategoryRepository productCategoryRepository;

  @PostMapping(path="/productcategory/add") 
  public @ResponseBody ResponseEntity<ProductCategory> addNewProductCategory (
      @RequestParam String name
      ) {
    
    try {
    	ProductCategory pcData = new ProductCategory();
    	pcData.setName(name);
        productCategoryRepository.save(pcData);
        return ResponseEntity.ok(pcData); 
	}catch (Exception e) {
		return ResponseEntity.notFound().build();
	}
  }

  @GetMapping(path="/productcategory/showall")
  public @ResponseBody ResponseEntity<Iterable<ProductCategory>> getAllProductCategory() {
	  try {
			return ResponseEntity.ok(productCategoryRepository.findAll()); 
		}catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
  }

  @GetMapping(path="/productcategory/{categoryid}")
  public @ResponseBody ResponseEntity<ProductCategory> getProductCategory(@PathVariable int categoryid) {
	try {
		ProductCategory pcData = productCategoryRepository.findById(categoryid).get();
		return ResponseEntity.ok(pcData); 
	}catch (Exception e) {
		return ResponseEntity.notFound().build();
	}
  }
  
  @PutMapping(path="/productcategory/update/{categoryid}")
  public @ResponseBody ResponseEntity<ProductCategory> updateProductCategory(
        @RequestParam String name,
        @RequestParam String description,
	    @PathVariable int categoryid) {

	  try {
        ProductCategory pcData = new ProductCategory();
        pcData.setCategoryId(categoryid);
    	pcData.setName(name);
		  productCategoryRepository.save(pcData);
		  return ResponseEntity.ok().build();
		}catch (Exception e) {
		  return ResponseEntity.notFound().build();
		}
  }
  
  @DeleteMapping(path="/productcategory/delete/{categoryid}")
  public @ResponseBody ResponseEntity<Void> deteleProductCategory(@PathVariable int categoryid){
	try {
	  productCategoryRepository.deleteById(categoryid);
	  return ResponseEntity.ok().build();
	}catch (Exception e) {
	  return ResponseEntity.notFound().build();
	} 
  }
}