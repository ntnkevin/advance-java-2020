package com.tubes.tokoFurnitur.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tubes.tokoFurnitur.model.Orders;
import com.tubes.tokoFurnitur.model.Payment;
import com.tubes.tokoFurnitur.repository.OrdersRepository;
import com.tubes.tokoFurnitur.repository.PaymentRepository;

@Controller
@RequestMapping(path="/tubes")
public class PaymentController {
  @Autowired
  private PaymentRepository paymentRepository;
  @Autowired
  private OrdersRepository orderRepository;

  @PostMapping(path="/payment/add") 
  public @ResponseBody ResponseEntity<Payment> addNewPayment (
        @RequestParam String amount,
        @RequestParam String paiddate,
        @RequestParam Integer ordersid
      ) {
    
    try {
    	Payment paymentData = new Payment();
    	Orders order = orderRepository.findById(ordersid).get();
    	paymentData.setAmount(amount);
        paymentData.setPaidDate(paiddate);
        paymentData.setOrdersid(order);
        
        paymentRepository.save(paymentData);
        return ResponseEntity.ok(paymentData); 
	}catch (Exception e) {
		return ResponseEntity.notFound().build();
	}
  }

  @GetMapping(path="/payment/showall")
  public @ResponseBody ResponseEntity<Iterable<Payment>> getAllPayment() {
	  try {
			return ResponseEntity.ok(paymentRepository.findAll()); 
		}catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
  }

  @GetMapping(path="/payment/{paymentid}")
  public @ResponseBody ResponseEntity<Payment> getPayment(@PathVariable int paymentid) {
	try {
		Payment paymentData = paymentRepository.findById(paymentid).get();
		return ResponseEntity.ok(paymentData); 
	}catch (Exception e) {
		return ResponseEntity.notFound().build();
	}
  }
  
  @PutMapping(path="/payment/update/{paymentid}")
  public @ResponseBody ResponseEntity<Payment> updatePayment(
      @PathVariable int paymentid,
      @RequestParam String amount,
      @RequestParam String paiddate,
      @RequestParam Integer ordersid) {

	  try {
        Payment paymentData = new Payment();
        Orders orders = orderRepository.findById(ordersid).get();
        paymentData.setPaymentId(paymentid);
    	paymentData.setAmount(amount);
        paymentData.setPaidDate(paiddate);
        paymentData.setOrdersid(orders);
        
        paymentRepository.save(paymentData);
		  return ResponseEntity.ok().build();
		}catch (Exception e) {
		  return ResponseEntity.notFound().build();
		}
  }
  
  @DeleteMapping(path="/payment/delete/{paymentid}")
  public @ResponseBody ResponseEntity<Void> detelePayment(@PathVariable int paymentid){
	try {
	  paymentRepository.deleteById(paymentid);
	  return ResponseEntity.ok().build();
	}catch (Exception e) {
	  return ResponseEntity.notFound().build();
	} 
  }
}