package com.tubes.tokoFurnitur.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tubes.tokoFurnitur.model.Customer;
import com.tubes.tokoFurnitur.model.Orders;
import com.tubes.tokoFurnitur.model.Shipment;
import com.tubes.tokoFurnitur.repository.CustomerRepository;
import com.tubes.tokoFurnitur.repository.OrdersRepository;
import com.tubes.tokoFurnitur.repository.ShipmentRepository;

@Controller
@RequestMapping(path="/tubes")
public class ShipmentController {
  @Autowired   
  private ShipmentRepository shipmentRepository;
  @Autowired
  private CustomerRepository customerRepository;
  @Autowired
  private OrdersRepository orderRepository;
  

  @PostMapping(path="/shipment/add") 
  public @ResponseBody ResponseEntity<Shipment> addNewShipment (
        @RequestParam String shipmentdate,
        @RequestParam String shipmentstatus,
        @RequestParam Integer customerid,
        @RequestParam Integer ordersid
      ) {
    
    try {
    	Shipment shipmentData = new Shipment();
    	Orders order = orderRepository.findById(ordersid).get();
    	Customer customer = customerRepository.findById(customerid).get();
    	shipmentData.setShipmentDate(shipmentdate);
        shipmentData.setShipmentStatus(shipmentstatus);
        shipmentData.setCustomerid(customer);
        shipmentData.setOrdersid(order);
        
        shipmentRepository.save(shipmentData);
        return ResponseEntity.ok(shipmentData); 
	}catch (Exception e) {
		return ResponseEntity.notFound().build();
	}
  }

  @GetMapping(path="/shipment/showall")
  public @ResponseBody ResponseEntity<Iterable<Shipment>> getAllShipment() {
	  try {
			return ResponseEntity.ok(shipmentRepository.findAll()); 
		}catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
  }

  @GetMapping(path="/shipment/{shipmentid}")
  public @ResponseBody ResponseEntity<Shipment> getShipment(@PathVariable int shipmentid) {
	try {
		Shipment shipmentData = shipmentRepository.findById(shipmentid).get();
		return ResponseEntity.ok(shipmentData); 
	}catch (Exception e) {
		return ResponseEntity.notFound().build();
	}
  }
  
  @PutMapping(path="/shipment/update/{shipmentid}")
  public @ResponseBody ResponseEntity<Shipment> updateShipment(
      @PathVariable int shipmentid,
      @RequestParam String shipmentdate,
      @RequestParam String shipmentstatus,
      @RequestParam Integer customerid,
      @RequestParam Integer ordersid) {

	  try {
        Shipment shipmentData = new Shipment();
        Orders order = orderRepository.findById(ordersid).get();
    	Customer customer = customerRepository.findById(customerid).get();
        shipmentData.setShipmentId(shipmentid);
    	shipmentData.setShipmentDate(shipmentdate);
        shipmentData.setShipmentStatus(shipmentstatus);
        shipmentData.setCustomerid(customer);
        shipmentData.setOrdersid(order);
        
        shipmentRepository.save(shipmentData);
		  return ResponseEntity.ok().build();
		}catch (Exception e) {
		  return ResponseEntity.notFound().build();
		}
  }
  
  @DeleteMapping(path="/shipment/delete/{shipmentid}")
  public @ResponseBody ResponseEntity<Void> deteleShipment(@PathVariable int shipmentid){
	try {
	  shipmentRepository.deleteById(shipmentid);
	  return ResponseEntity.ok().build();
	}catch (Exception e) {
	  return ResponseEntity.notFound().build();
	} 
  }
}