package com.tubes.tokoFurnitur.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ProductCategory {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer categoryid;
    private String name;

    @OneToMany(mappedBy="categoryid", fetch=FetchType.LAZY, targetEntity=Product.class)
    @JsonIgnore
    private Set<Product> products; 
    
    @JsonBackReference
    public Set<Product> getProducts(){
    	return products;
    }
    
    public void setProduct(Set<Product> products) {
    	this.products=products;
    	for(Product product : products) {
    		product.setCategoryid(this);
    	}
    }
    
    //categoryid
    public Integer getCategoryId() {
        return categoryid;
    }
    public void setCategoryId(Integer categoryid) {
        this.categoryid = categoryid;
    }

    //name
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }


}