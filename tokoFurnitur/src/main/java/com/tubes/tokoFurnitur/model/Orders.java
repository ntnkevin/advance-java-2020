package com.tubes.tokoFurnitur.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Orders {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer ordersid;
    private Integer qty;
    private String status;
    private String ordersdate;
    
    @OneToMany(mappedBy="ordersid", fetch=FetchType.LAZY, targetEntity=Payment.class)
    @JsonIgnore
    private Set<Payment> payments; 
    
    @JsonBackReference
    public Set<Payment> getProducts(){
    	return payments;
    }
    
    public void setPayment(Set<Payment> payment) {
    	this.payments=payment;
    	for(Payment payments : payment) {
    		payments.setOrdersid(this);
    	}
    }
    
    @OneToMany(mappedBy="ordersid", fetch=FetchType.LAZY, targetEntity=Shipment.class)
    @JsonIgnore
    private Set<Shipment> shipment; 
    
    @JsonBackReference
    public Set<Shipment> getShipments(){
    	return shipment;
    }
    
    
    @OneToMany(mappedBy="ordersid", fetch=FetchType.LAZY, targetEntity=ProductReview.class)
    @JsonIgnore
    private Set<ProductReview> productReview; 
    
    @JsonBackReference
    public Set<ProductReview> getProductsReview(){
    	return productReview;
    }
    
    public void setProductReview(Set<ProductReview> productReview) {
    	this.productReview=productReview;
    	for(ProductReview productReviews: productReview) {
    		productReviews.setOrdersid(this);
    	}
    }
    
    //productid
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="productid", nullable=true)
    private Product productid;
   
    public Product getProductid() {
		return productid;
	}
	public void setProductid(Product productid) {
		this.productid = productid;
	}
	//ordersid
    public Integer getOrdersId() {
        return ordersid;
    }
    public void setOrdersId(Integer ordersid) {
        this.ordersid = ordersid;
    }

    	//status
        public String getStatus() {
            return status;
        }
        public void setStatus(String status) {
            this.status = status;
        }

    
    //customerid
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="customerid", nullable=true)
    private Customer customerid;

    public Customer getCustomerid() {
		return customerid;
	}
	public void setCustomerid(Customer customerid) {
		this.customerid = customerid;
	}
	//ordersdate
    public String getOrdersDate() {
        return ordersdate;
    }
    public void setOrdersDate(String ordersdate) {
        this.ordersdate = ordersdate;
    }

    //price
    public Integer getQty() {
        return qty;
    }
    public void setQty(Integer qty) {
        this.qty = qty;
    }    
}