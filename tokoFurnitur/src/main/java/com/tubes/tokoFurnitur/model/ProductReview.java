package com.tubes.tokoFurnitur.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class ProductReview {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer reviewid;
    private String reviewdate;
    private String description;
    
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ordersid", nullable=true)
    private Orders ordersid;

	@JsonManagedReference
    public Orders getOrdersid() {
		return ordersid;
	}
	public void setOrdersid(Orders ordersid) {
		this.ordersid = ordersid;
	}
	//reviewid
    public Integer getReviewId() {
        return reviewid;
    }
    public void setReviewId(Integer reviewid) {
        this.reviewid = reviewid;
    }

    //reviewdate
    public String getReviewDate() {
        return reviewdate;
    }
    public void setReviewDate(String reviewdate) {
        this.reviewdate = reviewdate;
    }

    //description
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
 
}