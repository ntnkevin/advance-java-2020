package com.tubes.tokoFurnitur.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Shipment {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer shipmentid;
    private String shipmentdate;
    private String shipmentstatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="customerid", nullable=true)
	private Customer customerid;
    
    //customerid
	@JsonManagedReference
	public Customer getCustomerid() {
		return customerid;
	}
	public void setCustomerid(Customer customerid) {
		this.customerid = customerid;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ordersid", nullable=true)
    private Orders ordersid;

	//ordersid
	@JsonManagedReference
	public Orders getOrdersid() {
		return ordersid;
	}
	public void setOrdersid(Orders ordersid) {
		this.ordersid = ordersid;
	}

    //shipment
    public Integer getShipmentId() {
        return shipmentid;
    }
    public void setShipmentId(Integer shipmentid) {
        this.shipmentid = shipmentid;
    }


    //shipmentdate
    public String getShipmentDate() {
        return shipmentdate;
    }
    public void setShipmentDate(String shipmentdate) {
        this.shipmentdate = shipmentdate;
    }

    //shipmentstatus
    public String getShipmentStatus() {
        return shipmentstatus;
    }
    public void setShipmentStatus(String shipmentstatus) {
        this.shipmentstatus = shipmentstatus;
    }    
}