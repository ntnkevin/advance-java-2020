package com.tubes.tokoFurnitur.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Customer {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer customerid;
    private String phone;
    private String gender;
    private String name;
    private String address;

    @OneToMany(mappedBy="customerid", fetch=FetchType.LAZY, targetEntity=Orders.class)
    @JsonIgnore
    private Set<Orders> orders; 
    
    @JsonBackReference
    public Set<Orders> getProducts(){
    	return orders;
    }
    
    @OneToMany(mappedBy="customerid", fetch=FetchType.LAZY, targetEntity=Shipment.class)
    @JsonIgnore
    private Set<Shipment> shipment; 
    
    @JsonBackReference
    public Set<Shipment> getShipments(){
    	return shipment;
    }
    
    //customerid
    public Integer getCustomerId() {
        return customerid;
    }
    public void setCustomerId(Integer customerid) {
        this.customerid = customerid;
    }

    //phonenumber
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    //gender
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }

    //name
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    //address
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }    
}