package com.tubes.tokoFurnitur.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer productid;
    private String name;
    private String description;
    private String color;
    private Integer price;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="categoryid", nullable=true)
    private ProductCategory categoryid;

    @JsonManagedReference
    public ProductCategory getCategoryid() {
		return categoryid;
	}
	public void setCategoryid(ProductCategory categoryid) {
		this.categoryid = categoryid;
	}
	
    @OneToMany(mappedBy="productid", fetch=FetchType.LAZY, targetEntity=Orders.class)
    @JsonIgnore
    private Set<Orders> order; 
    
    @JsonBackReference
    public Set<Orders> getProducts(){
    	return order;
    }
    
	public Set<Orders> getOrder() {
		return order;
	}
	public void setOrder(Set<Orders> order) {
		this.order = order;
	}
	
	
	//productid
    public Integer getProductId() {
        return productid;
    }
    public void setProductId(Integer productid) {
        this.productid = productid;
    }

    //name
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    //color
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }

    //description
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    //price
    public Integer getPrice() {
        return price;
    }
    public void setPrice(Integer price) {
        this.price = price;
    }    
}