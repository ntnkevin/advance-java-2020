package com.tubes.tokoFurnitur.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Payment {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer paymentid;
    private String amount;
    private String paiddate;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ordersid", nullable=true)
    private Orders ordersid;
    @JsonManagedReference
    public Orders getOrderid() {
		return ordersid;
	}

    //orderid
    public void setOrdersid(Orders ordersid) {
		this.ordersid = ordersid;
	}
	public void setPaymentId(Integer paymentid) {
        this.paymentid = paymentid;
    }
	
	//paymentid
    public Integer getPaymentId() {
        return paymentid;
    }
    public Orders getOrdersid() {
		return ordersid;
	}
	


    //paiddate
    public String getPaidDate() {
        return paiddate;
    }
    public void setPaidDate(String paiddate) {
        this.paiddate = paiddate;
    }

    //amount
    public String getAmount() {
        return amount;
    }
    public void setAmount(String amount) {
        this.amount = amount;
    }    
}